package uk.co.hexeptionclients.jds.ui.themes;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;
import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.gui.JDSTabGui;
import uk.co.hexeptionclients.jds.module.Module;
import uk.co.hexeptionclients.jds.module.Module.Category;
import uk.co.hexeptionclients.jds.ui.InGameHud;
import uk.co.hexeptionclients.jds.wrapper.Wrapper;

public class JDSTheme implements InGameHud {
	
	private static final ResourceLocation LOGO = new ResourceLocation("jds/JDS.png");
	
	@Override
	public String themeName() {
		return "JDS";
	}
	
	@Override
	public void render(Minecraft minecraft, int displayWidth, int dispalyHeight) {
		ScaledResolution scaledResolution = new ScaledResolution(minecraft);
		
		GL11.glPushMatrix(); 
		GL11.glScalef(0.55f, 0.55f, 0.55f);
		Wrapper.getInstance().getMinecraft().getTextureManager().bindTexture(LOGO);
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		Gui.drawModalRectWithCustomSizedTexture(-18, 10, 0, 0, 256, 64, 256, 64);
		GL11.glPopMatrix();
		
		renderArrayList(scaledResolution);
		JDS.theClient.tabGui.drawTabGui();
	}
	
	private void renderArrayList(ScaledResolution scaledResolution){
		int yCount = 4;
		int right = scaledResolution.getScaledWidth() - 5;
		
		for(Module mod : JDS.theClient.moduleManager.getAllMods()){
			if(mod.getState() && mod.getCategory() != Category.GUI){
				JDS.theClient.fonts.Arial25.drawString(mod.getName(), right - Wrapper.getInstance().getFontRenderer().getStringWidth(mod.getName()), yCount, mod.getColor());
				yCount += 13;
			}
		}
	}
	
	@Override
	public void onKeyPressed(int key) {
		JDS.theClient.tabGui.actionEvent(key);
	}
	
}
