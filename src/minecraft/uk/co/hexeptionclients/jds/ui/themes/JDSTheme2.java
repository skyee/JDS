package uk.co.hexeptionclients.jds.ui.themes;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.module.Module;
import uk.co.hexeptionclients.jds.module.Module.Category;
import uk.co.hexeptionclients.jds.ui.InGameHud;
import uk.co.hexeptionclients.jds.wrapper.Wrapper;

public class JDSTheme2 implements InGameHud {
	
	@Override
	public String themeName() {
		return "JDS Testing";
	}
	
	@Override
	public void render(Minecraft minecraft, int displayWidth, int dispalyHeight) {
		ScaledResolution scaledResolution = new ScaledResolution(minecraft);
		GL11.glPushMatrix();
		GL11.glScaled(2,2, 2);
		//TODO: Make Better
		Wrapper.getInstance().getFontRenderer().drawStringWithShadow("�4[�r " + JDS.theClient.clientName + " �4]�r rel-�7" + JDS.theClient.clientVersion, 4, 4, 0x8000ffff);
		GL11.glPopMatrix();
		
		renderArrayList(scaledResolution);
	}
	
	private void renderArrayList(ScaledResolution scaledResolution){
		int yCount = 4;
		int right = scaledResolution.getScaledWidth() - 3;
		
		for(Module mod : JDS.theClient.moduleManager.getAllMods()){
			if(mod.getState() && mod.getCategory() != Category.GUI){
				Wrapper.getInstance().getFontRenderer().drawStringWithShadow(mod.getName(), right - Wrapper.getInstance().getFontRenderer().getStringWidth(mod.getName()), yCount, mod.getColor());
				yCount += 10;
			}
		}
	}
	
	@Override
	public void onKeyPressed(int key) {
		// TODO: Tab Gui!
	}
	
}
