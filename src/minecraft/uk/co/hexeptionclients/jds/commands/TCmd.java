package uk.co.hexeptionclients.jds.commands;

import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.commands.Cmd.CmdInfo;
import uk.co.hexeptionclients.jds.module.Module;

@CmdInfo(help = "Toggles a mod.", name = "t", syntax = { "<mod> [(on|off)]" })
public class TCmd extends Cmd{

	@Override
	public void execute(String[] args) throws Error {
		int mode = -1;
		if (args.length == 1)
			mode = 0;
		else if (args.length == 2 && args[1].equalsIgnoreCase("on"))
			mode = 1;
		else if (args.length == 2 && args[1].equalsIgnoreCase("off"))
			mode = 2;
		else syntaxError();
		Module mod = JDS.theClient.moduleManager.getModByName(args[0]);
		if (mod == null)
			error("Could not find mod \"" + args[0] + "\".");
		if (mode == 0)
			mod.toggle();
		else if (mode == 1 && !mod.getState())
			mod.setState(true);
		else if (mode == 2 && mod.getState())
			mod.setState(false);
	}
	
}
