package uk.co.hexeptionclients.jds.module;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventTarget;

import net.minecraft.util.text.TextFormatting;
import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.events.EventTick;
import uk.co.hexeptionclients.jds.irc.IrcChatLine;
import uk.co.hexeptionclients.jds.irc.IrcManager;
import uk.co.hexeptionclients.jds.module.Module.Category;
import uk.co.hexeptionclients.jds.module.Module.Info;
import uk.co.hexeptionclients.jds.wrapper.Wrapper;

@Info(category = Category.MISC, desciption = "Talk in irc.", name = "Irc")
public class IRCMod extends Module {
	
	private IrcManager irc = JDS.theClient.ircManager;
	
	@Override
	public void onEnable() {
		Wrapper.getInstance().addIRCMessage("Use '@�7{�3Message�7}' to chat");
		Wrapper.getInstance().addIRCMessage("You will be know as: �3" + irc.getNick());
	}
	
	@EventTarget
	private void onTick(EventTick event){
		if(irc.newMessages()){
			for(IrcChatLine irc1 : irc.getUnreadLines()){
				if(irc1.getSender().equals(irc.getName())){
					irc1.setSender("You");
				}
				
				Wrapper.getInstance().addIRCMessage(TextFormatting.DARK_AQUA + "[" + TextFormatting.GRAY + irc1.getSender() + TextFormatting.DARK_AQUA + "] " + irc1.getLine());
				irc1.setRead(true);
			}
		}
	}
}
