package uk.co.hexeptionclients.jds.module;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventTarget;

import uk.co.hexeptionclients.jds.events.EventUpdate;
import uk.co.hexeptionclients.jds.module.Module.Category;
import uk.co.hexeptionclients.jds.module.Module.Info;
import uk.co.hexeptionclients.jds.wrapper.Wrapper;

@Info(category = Category.MOVMENT,  desciption = "Walks Up Blocks.", name = "Step")
public class StepMod extends Module{
	
	@EventTarget
	public void onUpdate(EventUpdate event){
		if(getState()){
			Wrapper.getInstance().getPlayer().stepHeight = 1;
		}
	}
	
	@Override
	public void onDisable() {
		Wrapper.getInstance().getPlayer().stepHeight = 0.5f;
	}
	
}
