package uk.co.hexeptionclients.jds.capes;

import java.io.InputStreamReader;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.minecraft.MinecraftProfileTexture.Type;

import uk.co.hexeptionclients.jds.JDS;

public class Capes {
	
	public static JsonObject capesJson;
	private static JsonParser jsonParser = new JsonParser();
	
	public static void getCapes(GameProfile gameProfile, Map skinmap){
		if(capesJson == null){
			try{
				HttpsURLConnection connection = (HttpsURLConnection) new URL(JDS.theClient.clientCapeUrl).openConnection();
				connection.connect();
				capesJson = jsonParser.parse(new InputStreamReader(connection.getInputStream())).getAsJsonObject();
			}catch (Exception e) {
				JDS.theClient.clientLogger.error("[JDS] Failed to load cape json");
				e.printStackTrace();
				return;
			}
		}
		
		try{
			if(capesJson.has(gameProfile.getName())){
				skinmap.put(Type.CAPE, new MinecraftProfileTexture(capesJson.get(gameProfile.getName()).getAsString(), null));
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
