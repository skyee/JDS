package uk.co.hexeptionclients.jds;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.darkmagician6.eventapi.EventManager;

import net.minecraft.client.Minecraft;
import uk.co.hexeptionclients.jds.commands.CmdManager;
import uk.co.hexeptionclients.jds.files.FileManager;
import uk.co.hexeptionclients.jds.fonts.JDSFontManager;
import uk.co.hexeptionclients.jds.friend.FriendManager;
import uk.co.hexeptionclients.jds.gui.JDSTabGui;
import uk.co.hexeptionclients.jds.irc.IrcManager;
import uk.co.hexeptionclients.jds.keybinds.KeybindsManager;
import uk.co.hexeptionclients.jds.module.ModuleManager;
import uk.co.hexeptionclients.jds.ui.Hud;

public enum JDS {
	
	theClient;
	
	private String website = "www.hexeptionclients.co.uk/";
	public String clientName = "JDS";
	public Float clientVersion = 1.0F;
	public String clientCreator = "Hexeption";
	public String clientChangelog = website + clientName + "/changelog";
	public String clientDownload = website + clientName + "/download";
	public String clientCapeUrl = "https://www.dropbox.com/s/6e5egb5jjht28cr/capes.json?raw=1";
	
	public Hud hud;
	public ModuleManager moduleManager;
	public IrcManager ircManager;
	public CmdManager cmdManager;
	public FileManager fileManager;
	public FriendManager friendManager;
	public KeybindsManager keybindsManager;
	public JDSFontManager fonts;
	public JDSTabGui tabGui;
	
	public Logger clientLogger = LogManager.getLogger();
	
	public void StartClient() {
		clientLogger.info("========================================================");
		clientLogger.info("Starting '" + clientName + "', Version '" + clientVersion + "', Created by '" + clientCreator + "'.");
		clientLogger.info("========================================================");
		EventManager.register(this);
		ircManager = new IrcManager(Minecraft.getMinecraft().getSession().getUsername());
		ircManager.connect();
		
		keybindsManager = new KeybindsManager();
		moduleManager = new ModuleManager();
		hud = new Hud();
		cmdManager = new CmdManager();
		fileManager = new FileManager();
		friendManager = new FriendManager();
		fonts = new JDSFontManager();
		tabGui = new JDSTabGui();
	
		hud.loadThemes();
		fileManager.init();
		fonts.loadFonts();
	}	
}
