package uk.co.hexeptionclients.jds.gui;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.module.Module;
import uk.co.hexeptionclients.jds.module.Module.Category;
import uk.co.hexeptionclients.jds.utils.GuiUtils;

public class JDSTabGui {
	
	private ArrayList<String> mods = new ArrayList();
	private ArrayList<String> category = new ArrayList();
	
	private int selectedMod, selectedTab;
	
	private int start, start1 = start + 14, end;
	
	public static int screen = 0;
	
	public JDSTabGui() {
		Category[] arrayofCategorys;
		int j = (arrayofCategorys = Category.values()).length;
		
		for (int i = 0; i < j; i++) {
			Category category = arrayofCategorys[i];
			
			if (category.name().equalsIgnoreCase("Gui")) {
				continue;
			}
			
			this.category.add(category.toString().substring(0, 1) + category.toString().substring(1, category.toString().length()).toLowerCase());
		}
	}
	
	public void drawTabGui() {
		
		int count = 0;
		
		for(Category c : Category.values()){
			if(!c.name().equalsIgnoreCase("Gui")){
				int y = 52 + (count * 18);
                int yZ = 52 + ((count + 1) * 13);
                
                GuiUtils.drawBorderRect(1, y, 55, y + 17, !c.name().equalsIgnoreCase(category.get(this.selectedTab)) ? 0xba681aa9 : 0xba210d26, !c.name().equalsIgnoreCase(category.get(this.selectedTab)) ? 0xba210d26 : 0xba681aa9, 1);
                count++;
			}
		}
		
		int categoryCount = 0;
		
		for(String s : this.category){
			JDS.theClient.fonts.Arial20.drawCenteredString(s, 27, 54 + categoryCount * 18, -1);
			categoryCount++;
		}
		
		if(screen == 1){
			int modCount = 0;
			
			for(Module mod: getModsForCategory()){
				String color;
				if(mod.getState()){
					color = "\247f";
				}else{
 					color = "\2477";
				}
				
				String name = color + mod.getName();
				int y = 52 + (modCount * 18);
                int yZ = 62 + ((modCount + 1) * 13); 
                GuiUtils.drawBorderRect(58, y, 62 + this.getLongestModWidth(), y + 15, !mod.getName().equalsIgnoreCase(this.getModsForCategory().get(this.selectedMod).getName()) ? 0xba681aa9 : 0xba210d26 , !mod.getName().equalsIgnoreCase(this.getModsForCategory().get(this.selectedMod).getName()) ? 0xba681aa9 : 0xba210d23, 1);//                GuiUtils.drawBorderRect(58, y, 62 + this.getNameLongestModWidth(), y + 15, !mod.getName().equalsIgnoreCase(category.get(this.selectedTab)) ? 0xba681aa9 : 0xba210d26, !mod.getName().equalsIgnoreCase(category.get(this.selectedTab)) ? 0xba210d26 : 0xba, 1);
               JDS.theClient.fonts.Arial20.drawCenteredString(name, 59 + (this.getLongestModWidth() / 2), y + 1, 0xFFFFFFFF);
               modCount++;
			}
		}
		
	}
	
	private void up() {
		if (screen == 0) {
			if (this.selectedTab <= 0) {
				this.selectedTab = this.category.size();
			}
			this.selectedTab -= 1;
		} else if (screen == 1) {
			if (this.selectedMod <= 0) {
				this.selectedMod = getModsForCategory().size();
			}
			
			this.selectedMod -= 1;
		}
	}
	
	private void down() {
		if (screen == 0) {
			if (this.selectedTab >= this.category.size() - 1) {
				this.selectedTab = -1;
			}
			this.selectedTab += 1;
		} else if (screen == 1) {
			if (this.selectedMod >= getModsForCategory().size() - 1) {
				this.selectedMod = -1;
			}
			
			this.selectedMod += 1;
		}
	}
	
	private void left() {
		if (screen == 1) {
			screen = 0;
		}
	}
	
	private void right() {
		if (screen == 1) {
			this.enter();
		} else {
			if (screen == 0) {
				screen = 1;
				this.selectedMod = 0;
			}
		}
	}
	
	private void enter() {
		if (screen == 1) {
			((Module) getModsForCategory().get(this.selectedMod)).toggle();
		}
	}
	
	public void actionEvent(final int key) {
		if (key == Keyboard.KEY_UP) {
			this.up();
		}
		
		if (key == Keyboard.KEY_DOWN) {
			this.down();
		}
		
		if (key == Keyboard.KEY_LEFT) {
			this.left();
		}
		
		if (key == Keyboard.KEY_RIGHT) {
			this.right();
		}
		
		if (key == Keyboard.KEY_RETURN) {
			this.enter();
		}
	}
	
	private ArrayList<Module> getModsForCategory() {
		ArrayList<Module> modss = new ArrayList<Module>();
		
		for (Module mod : JDS.theClient.moduleManager.getAllMods()) {
			if (mod.getCategory() == Category.valueOf((String) this.category.get(this.selectedTab).toUpperCase())) {
				modss.add(mod);
			}
		}
		
		return modss;
	}
	
	private int getLongestModWidth(){
		int longest = 0;
		
		for(Module mod : getModsForCategory()){
			if(JDS.theClient.fonts.Arial20.getWidth(mod.getName()) + 3 > longest){
				longest = (int) JDS.theClient.fonts.Arial20.getWidth(mod.getName()) + 3;
			}
		}
		
		return longest;
	}
	
}
