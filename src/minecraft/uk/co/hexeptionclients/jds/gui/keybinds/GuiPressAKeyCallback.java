package uk.co.hexeptionclients.jds.gui.keybinds;

public interface GuiPressAKeyCallback {
	public void setKey(String key);
}
