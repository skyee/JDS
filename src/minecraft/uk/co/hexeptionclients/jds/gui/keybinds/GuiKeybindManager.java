package uk.co.hexeptionclients.jds.gui.keybinds;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.TreeSet;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNo;
import uk.co.hexeptionclients.jds.JDS;
import uk.co.hexeptionclients.jds.keybinds.KeybindsManager;

public class GuiKeybindManager extends GuiScreen {
	private GuiScreen prevMenu;
	public static GuiKeybindSlot keybindslot;
	
	public GuiKeybindManager(GuiScreen par1GuiScreen) {
		prevMenu = par1GuiScreen;
	}
	
	@Override
	public void initGui() {
		keybindslot = new GuiKeybindSlot(mc, this);
		keybindslot.registerScrollButtons(7, 8);
		keybindslot.elementClicked(-1, false, 0, 0);
		
		buttonList.clear();
		buttonList.add(new GuiButton(0, width / 2 - 102, height - 52, 100, 20, "Add"));
		buttonList.add(new GuiButton(1, width / 2 + 2, height - 52, 100, 20, "Edit"));
		buttonList.add(new GuiButton(2, width / 2 - 102, height - 28, 100, 20, "Remove"));
		buttonList.add(new GuiButton(3, width / 2 + 2, height - 28, 100, 20, "Back"));
		buttonList.add(new GuiButton(4, 8, 8, 100, 20, "Reset Keybinds"));
	}
	
	public void updateScreen() {
		((GuiButton) buttonList.get(1)).enabled = keybindslot.getSelectedSlot() != -1;
		((GuiButton) buttonList.get(2)).enabled = keybindslot.getSelectedSlot() != -1;
	}
	
	protected void actionPerformed(GuiButton clickedButton) {
		if (clickedButton.enabled)
			if (clickedButton.id == 0)
				mc.displayGuiScreen(new GuiKeybindChange(this, null));
			else if (clickedButton.id == 3)
				mc.displayGuiScreen(prevMenu);
			else if (clickedButton.id == 4)
				mc.displayGuiScreen(new GuiYesNo(this, "Are you sure you want to reset your keybinds?", "This cannot be undone!", 0));
			else {
				if (keybindslot.getSelectedSlot() > JDS.theClient.keybindsManager.size())
					keybindslot.elementClicked(JDS.theClient.keybindsManager.size(), false, 0, 0);
				if (clickedButton.id == 1) {
					Entry<String, TreeSet<String>> entry = JDS.theClient.keybindsManager.entrySet().toArray(new Entry[JDS.theClient.keybindsManager.size()])[keybindslot.getSelectedSlot()];
					mc.displayGuiScreen(new GuiKeybindChange(this, entry));
				} else if (clickedButton.id == 2) {
					Entry<String, String> entry = JDS.theClient.keybindsManager.entrySet().toArray(new Entry[JDS.theClient.keybindsManager.size()])[keybindslot.getSelectedSlot()];
					JDS.theClient.keybindsManager.remove(entry.getKey());
					// WurstClient.INSTANCE.files.saveKeybinds();
				}
			}
	}
	
	@Override
	public void confirmClicked(boolean par1, int par2) {
		if (par1) {
			JDS.theClient.keybindsManager = new KeybindsManager();
			// JDS.theClient.fileManager.saveKeybinds();
		}
		mc.displayGuiScreen(this);
	}
	
	protected void keyTyped(char par1, int par2) {
		if (par2 == 28 || par2 == 156)
			actionPerformed((GuiButton) buttonList.get(0));
	}
	
	protected void mouseClicked(int par1, int par2, int par3) throws IOException {
		if (par2 >= 36 && par2 <= height - 57)
			if (par1 >= width / 2 + 140 || par1 <= width / 2 - 126)
				keybindslot.elementClicked(-1, false, 0, 0);
		super.mouseClicked(par1, par2, par3);
	}
	
	public void handleMouseInput() throws IOException {
		super.handleMouseInput();
		keybindslot.handleMouseInput();
	}
	
	public void drawScreen(int par1, int par2, float par3) {
		drawDefaultBackground();
		keybindslot.drawScreen(par1, par2, par3);
		drawCenteredString(fontRendererObj, "Keybind Manager", width / 2, 8, 16777215);
		drawCenteredString(fontRendererObj, "Keybinds: " + JDS.theClient.keybindsManager.size(), width / 2, 20, 16777215);
		super.drawScreen(par1, par2, par3);
	}
}
